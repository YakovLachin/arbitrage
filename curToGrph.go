package main

import (
	"gitlab.com/YakovLachin/gostructures"
	"gitlab.com/YakovLachin/arbitrage/currency"
	"math"
)

//
//func CurToGrph(curs []currency.Currency) gostructures.GraphInterface {
//	graph := gostructures.NewGraph()
//
//	ln := len(curs)
//	for i:= 0; i < ln; i++ {
//		node := gostructures.NewGraphNode(curs[i].ID, curs[i])
//		graph.AddNode(node)
//	}
//
//	for i:= 0; i < ln; i++ {
//		cur := curs[i]
//		courses := cur.GetCourses()
//
//		course, err := courses.Pop()
//		for err != nil {
//			edge := gostructures.NewEdge(graph.GetNode(i), graph.GetNode(course.Currency))
//			edge.SetWeight(-math.Log2(course.Value))
//			graph.AddEdge(edge)
//			course, err = courses.Pop()
//		}
//	}
//
//	return graph
//}
