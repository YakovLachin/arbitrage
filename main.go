package main

import (
	"gitlab.com/YakovLachin/arbitrage/currency"
	"fmt"
	"gitlab.com/YakovLachin/gostructures"
	"math"
)


func main() {
	curs := make([]currency.Currency, 3)

	rub := currency.NewCurrency(currency.RUB)
	rub.PushCourse(currency.NewCourse(currency.EUR, 70.6443))
	rub.PushCourse(currency.NewCourse(currency.EUR, 58.7503))
	curs[rub.ID] = rub

	eur := currency.NewCurrency(currency.EUR)
	eur.PushCourse(currency.NewCourse(currency.RUB, 0.0142))
	eur.PushCourse(currency.NewCourse(currency.USD, 105.374))
	curs[eur.ID] = eur

	usd := currency.NewCurrency(currency.USD)
	usd.PushCourse(currency.NewCourse(currency.RUB, 0.017))
	usd.PushCourse(currency.NewCourse(currency.EUR, 0.7292))

	curs[usd.ID] = usd

	fmt.Println(curs)
	graph := CurToGrph(curs)

	res := graph.FindNegativeWeightCycleFrom(rub.ID)
	fmt.Println(res)
}


func CurToGrph(curs []currency.Currency) gostructures.GraphInterface {
	graph := gostructures.NewGraph()

	ln := len(curs)
	for i:= 0; i < ln; i++ {
		node := gostructures.NewGraphNode(curs[i].ID, curs[i])
		graph.AddNode(node)
	}

	for i:= 0; i < ln; i++ {
		cur := curs[i]
		courses := cur.GetCourses()

		course, err := courses.Pop()
		for err != nil {
			edge := gostructures.NewEdge(graph.GetNode(i), graph.GetNode(course.Currency))
			edge.SetWeight(-math.Log2(course.Value))
			graph.AddEdge(edge)
			course, err = courses.Pop()
		}
	}

	return graph
}
