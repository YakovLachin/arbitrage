package currency

const (
	RUB = 0
	USD = 1
	EUR = 2
	JPY = 3
)

type Currency struct {
	ID      int
	Courses CourseStackInterface
}

type Course struct {
	Currency int
	Value    float64
}

func NewCurrency(ID int) Currency {
	return Currency{
		ID: ID,
		Courses: NewCourseStack(),
	}
}

func NewCourse(cur int, val float64) Course {
	return Course{
		Currency: cur,
		Value:    val,
	}
}

func (c *Currency) GetCourses() CourseStackInterface {
	return c.Courses
}

func (c *Currency) PushCourse(course Course) error {
	return c.Courses.Push(course)
}
