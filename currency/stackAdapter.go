package currency

import "gitlab.com/YakovLachin/gostructures"

type CourseStackInterface interface {
	Push(val Course) error
	Pop() (Course, error)
	Top() (Course, error)
}

type CourseStackAdapter struct {
	gostructures.StackInterface
}

func NewCourseStack() CourseStackInterface {
	return &CourseStackAdapter{
		StackInterface: gostructures.NewArrayStack(),
	}
}

func (a *CourseStackAdapter) Push(val Course) error {
	return a.StackInterface.Push(val)
}

func (a *CourseStackAdapter) Pop() (Course, error) {
	res, err := a.StackInterface.Pop()

	return res.(Course), err

}

func (a *CourseStackAdapter) Top() (Course, error) {
	res, err := a.StackInterface.Top()

	return res.(Course), err

}

type CurrencyStackInterface interface {
	Push(val Currency) error
	Pop() (Currency, error)
	Top() (Currency, error)
}

type CurrencyStackAdapter struct {
	gostructures.StackInterface
}

func NewCurrencyStack() CurrencyStackInterface {
	return &CurrencyStackAdapter{
		StackInterface: gostructures.NewArrayStack(),
	}
}

func (a *CurrencyStackAdapter) Push(val Currency) error {
	return a.StackInterface.Push(val)
}

func (a *CurrencyStackAdapter) Pop() (Currency, error) {
	res, err := a.StackInterface.Pop()

	return res.(Currency), err
}

func (a *CurrencyStackAdapter) Top() (Currency, error) {
	res, err := a.StackInterface.Top()

	return res.(Currency), err
}
