package currency

import "testing"

func TestCurrency(t *testing.T) {
	eur := NewCurrency(EUR)
	eur.pushCourse(NewCourse(RUB, 0.0142))
	eur.pushCourse(NewCourse(USD, 105.374))

	rub := NewCurrency(RUB)
	rub.pushCourse(NewCourse(EUR, 70.6443))
	rub.pushCourse(NewCourse(EUR, 58.7503))

	usd := NewCurrency(USD)
	usd.pushCourse(NewCourse(RUB, 0.017))
	usd.pushCourse(NewCourse(EUR, 0.7292))
}
